/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.common.util;

<<<<<<< HEAD
import java.util.Properties;

=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
/**
 * Convenient class for property extraction.
 * 
=======
import java.util.List;
import java.util.Properties;

import static org.ngrinder.common.util.Preconditions.checkNotNull;

/**
 * Convenient class for property extraction.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 */
public class PropertiesWrapper {
	private final Properties properties;
<<<<<<< HEAD
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesWrapper.class);

	private static final String DEFAULT_ERROR_MESSGAE = "The {} is not defined in conf file. Use {} instead.";

	/**
	 * Constructor.
	 * 
	 * @param properties
	 *            {@link Properties} which will be used for data retrieval.
	 */
	public PropertiesWrapper(Properties properties) {
		this.properties = properties;
	}

	/**
	 * Get property.
	 * 
	 * @param key
	 *            property key
	 * @param defaultValue
	 *            default value when data is not available
	 * @param errorMsgTemplate
	 *            error msg
	 * @return property value
	 */
	public String getProperty(String key, String defaultValue, String errorMsgTemplate) {
		String value = this.properties.getProperty(key);
		if (StringUtils.isBlank(value)) {
			LOGGER.trace(errorMsgTemplate, key, defaultValue);
			value = defaultValue;
		} else {
			value = value.trim();
		}
		return value;
	}

	/**
	 * Get the property for the given property key.
	 * 
	 * @param key
	 *            property key
	 * @param defaultValue
	 *            default value when data is not available
	 * @return property value
	 */
	public String getProperty(String key, String defaultValue) {
		return StringUtils.trim(getProperty(key, defaultValue, DEFAULT_ERROR_MESSGAE));
	}

	/**
	 * Get the property for the given property key considering with backward
	 * compatibility.
	 * 
	 * @param key
	 *            property key
	 * @param oldKey
	 *            old property key.
	 * @param defaultValue
	 *            default value when data is not available
	 * @return property value
	 */
	public String getPropertyWithBackwardCompatibility(String key, String oldKey, String defaultValue) {
		String property = getProperty(key, "", DEFAULT_ERROR_MESSGAE);
		if (StringUtils.isEmpty(property)) {
			property = getProperty(oldKey, defaultValue, DEFAULT_ERROR_MESSGAE);
		}
		return StringUtils.trim(property);
	}

	/**
	 * Add property.
	 * 
	 * @param key
	 *            property key
	 * @param value
	 *            property value
=======
	@SuppressWarnings("UnusedDeclaration")
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesWrapper.class);
	private PropertiesKeyMapper propertiesKeyMapper;

	/**
	 * Constructor.
	 *
	 * @param properties {@link Properties} which will be used for data retrieval.
	 */
	public PropertiesWrapper(Properties properties, PropertiesKeyMapper propertiesKeyMapper) {
		this.properties = properties;
		this.propertiesKeyMapper = propertiesKeyMapper;
	}

	public boolean exist(String key) {
		String value = this.properties.getProperty(key);
		if (value == null) {
			List<String> keys = propertiesKeyMapper.getKeys(key);
			if (keys != null && !keys.isEmpty()) {
				for (String each : keys) {
					value = this.properties.getProperty(each);
					if (value != null) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Get the property.
	 *
	 * @param key property key
	 * @return property value
	 */
	public String getProperty(String key) {
		String value = this.properties.getProperty(key);
		if (StringUtils.isNotBlank(value)) {
			return value.trim();
		}
		List<String> keys = propertiesKeyMapper.getKeys(key);

		for (String each : checkNotNull(keys, key + " should be exists")) {
			value = this.properties.getProperty(each);
			if (StringUtils.isNotBlank(value)) {
				return value.trim();
			}
		}
		return propertiesKeyMapper.getDefaultValue(key);
	}


	public String getProperty(String key, String defaultValue) {
		try {
			String property = getProperty(key);
			return StringUtils.isEmpty(property) ? defaultValue : property;
		} catch (IllegalArgumentException e) {
			return defaultValue;
		}
	}


	public int getPropertyInt(String key, int defaultValue) {
		try {
			return getPropertyInt(key);
		} catch (IllegalArgumentException e) {
			return defaultValue;
		}
	}

	public boolean getPropertyBoolean(String key, boolean defaultValue) {
		try {
			return getPropertyBoolean(key);
		} catch (IllegalArgumentException e) {
			return defaultValue;
		}
	}

	/**
	 * Add the property.
	 *
	 * @param key   property key
	 * @param value property value
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void addProperty(String key, String value) {
		this.properties.put(key, value);
	}

	/**
	 * Get property as integer.
<<<<<<< HEAD
	 * 
	 * @param key
	 *            property key
	 * @param defaultValue
	 *            default value when data is not available
	 * @return property integer value
	 */
	public int getPropertyInt(String key, int defaultValue) {
		String property = getProperty(key, String.valueOf(defaultValue), DEFAULT_ERROR_MESSGAE);
		return NumberUtils.toInt(property, defaultValue);
	}

	/**
	 * Get property as boolean.
	 * 
	 * @param key
	 *            property key
	 * @param defaultValue
	 *            default value when data is not available
	 * @return property boolean value
	 */
	public boolean getPropertyBoolean(String key, boolean defaultValue) {
		String property = getProperty(key, String.valueOf(defaultValue), DEFAULT_ERROR_MESSGAE);
		return BooleanUtils.toBoolean(property);
	}

	/**
	 * Set property.
	 * 
	 * @param key
	 *            key
	 * @param value
	 *            value to be stored.
	 */
	public void setProperty(String key, String value) {
		this.properties.setProperty(key, value);
	}
=======
	 *
	 * @param key property key
	 * @return property integer value
	 */
	public int getPropertyInt(String key) {
		return NumberUtils.toInt(getProperty(key));
	}

	/**
	 * Get the property as boolean.
	 *
	 * @param key property key
	 * @return property boolean value
	 */
	public boolean getPropertyBoolean(String key) {
		return BooleanUtils.toBoolean(getProperty(key));
	}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
