/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.extension;

import org.ngrinder.model.PerfTest;
import org.ngrinder.service.IPerfTestService;

/**
 * Plugin extension point for {@link PerfTest} start and finish.
 * 
<<<<<<< HEAD
 * This plugin is necessary if you want to notify the test start and end.
=======
 * This plugin is necessary if you want to be notified whenever the test start and end.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * 
 * @author JunHo Yoon
 * @since 3.0
 */
public interface OnTestLifeCycleRunnable {

	/**
	 * Callback method which will be invoked whenever {@link PerfTest} is started.
	 * 
<<<<<<< HEAD
	 * @param perfTest
	 *            Performance Test
	 * @param perfTestService
	 *            perfTestService interface
	 * @param version
	 *            ngrinder version
=======
	 * @param perfTest			Performance Test
	 * @param perfTestService	perfTestService interface
	 * @param version			ngrinder version
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void start(PerfTest perfTest, IPerfTestService perfTestService, String version);

	/**
	 * Callback method which will be invoked whenever {@link PerfTest} is finished.
	 * 
	 * 
<<<<<<< HEAD
	 * @param perfTest
	 *            Performance Test
	 * @param stopReason
	 *            stop reason
	 * @param perfTestService
	 *            perfTestService interface
	 * @param vesion
	 *            ngrinder version
	 */
	public void finish(PerfTest perfTest, String stopReason, IPerfTestService perfTestService, String vesion);
=======
	 * @param perfTest			Performance Test
	 * @param stopReason		stop reason
	 * @param perfTestService	perfTestService interface
	 * @param version			ngrinder version
	 */
	public void finish(PerfTest perfTest, String stopReason, IPerfTestService perfTestService, String version);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
}
