/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package net.grinder;

<<<<<<< HEAD
import static org.ngrinder.common.util.ExceptionUtils.processException;
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import net.grinder.common.GrinderException;
import net.grinder.common.GrinderProperties;
import net.grinder.communication.CommunicationDefaults;
import net.grinder.engine.agent.Agent;
import net.grinder.engine.agent.AgentImplementationEx;
import net.grinder.util.ListenerSupport;
import net.grinder.util.ListenerSupport.Informer;
<<<<<<< HEAD

import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.util.ThreadUtil;
=======
import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.util.ThreadUtils;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.infra.AgentConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

<<<<<<< HEAD
/**
 * Agent Daemon wrapper for {@link AgentImplementationEx} in thread.
 * 
=======
import static org.ngrinder.common.util.ExceptionUtils.processException;

/**
 * Agent Daemon wrapper for {@link AgentImplementationEx} in thread.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.0
 */
public class AgentDaemon implements Agent {
	private volatile AgentImplementationEx agent;
	private Thread thread = new Thread();
	private GrinderProperties properties;
	private final ListenerSupport<AgentShutDownListener> m_listeners = new ListenerSupport<AgentShutDownListener>();
<<<<<<< HEAD
	private boolean forceToshutdown = false;
	public static final Logger LOGGER = LoggerFactory.getLogger(AgentDaemon.class);
=======
	private boolean forceShutdown = false;
	public static final Logger LOGGER = LoggerFactory.getLogger("agent daemon");
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private final AgentConfig m_agentConfig;

	/**
	 * Constructor.
<<<<<<< HEAD
	 * 
	 * @param agentConfig
	 *            agent configuration
=======
	 *
	 * @param agentConfig agent configuration
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */

	public AgentDaemon(AgentConfig agentConfig) {
		this.m_agentConfig = agentConfig;
		try {
			properties = new GrinderProperties(GrinderProperties.DEFAULT_PROPERTIES);
		} catch (GrinderException e) {
<<<<<<< HEAD
			throw processException("Exception occurred while creating AgentDaemon", e);
=======
			throw processException("Exception occurred while creating agent daemon", e);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
	 * Set agent.
<<<<<<< HEAD
	 * 
	 * @param agent
	 *            agent
=======
	 *
	 * @param agent agent
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return set agent
	 */
	public synchronized AgentImplementationEx setAgent(AgentImplementationEx agent) {
		this.agent = agent;
		return this.agent;
	}

	/**
	 * Run agent to connect to default console in localhost.
	 */
	public void run() {
		run(null, CommunicationDefaults.CONSOLE_PORT);
	}

	/**
	 * Run agent to connect to given console in localhost.
<<<<<<< HEAD
	 * 
	 * @param consolePort
	 *            port to which agent connect
=======
	 *
	 * @param consolePort port to which agent connect
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void run(int consolePort) {
		run(null, consolePort);
	}

	/**
	 * Run agent with given {@link GrinderProperties}.
<<<<<<< HEAD
	 * 
	 * @param grinderProperties
	 *            {@link GrinderProperties}
=======
	 *
	 * @param grinderProperties {@link GrinderProperties}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void run(GrinderProperties grinderProperties) {
		this.properties = grinderProperties;
		run(null, 0);
	}

	/**
<<<<<<< HEAD
	 * Run agent with given consoleHost and consolePort. <br/>
	 * if consoleHost is null it will use localhost or use console host set in
	 * {@link GrinderProperties}
	 * 
	 * if port number is 0, it will use default consolePort or use console port
	 * set in {@link GrinderProperties}
	 * 
	 * @param consoleHost
	 *            host name
	 * @param consolePort
	 *            port number
	 * 
=======
	 * Run agent with given consoleHost and consolePort.
	 *
	 * if consoleHost is null it will use localhost or use console host set in
	 * {@link GrinderProperties}
	 *
	 * if port number is 0, it will use default consolePort or use console port
	 * set in {@link GrinderProperties}
	 *
	 * @param consoleHost host name
	 * @param consolePort port number
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void run(String consoleHost, int consolePort) {
		if (StringUtils.isNotEmpty(consoleHost)) {
			getGrinderProperties().setProperty(GrinderProperties.CONSOLE_HOST, consoleHost);
		}
		if (consolePort > 0) {
			getGrinderProperties().setInt(GrinderProperties.CONSOLE_PORT, consolePort);
		}

<<<<<<< HEAD
		thread = new Thread(new AgentThreadRunnable(), "Agent conntected to port : "
				+ getGrinderProperties().getInt(GrinderProperties.CONSOLE_PORT, 0));
		thread.setDaemon(true);
		thread.start();
		LOGGER.info("Agent Daemon {} is started.", thread.getName());
=======
		thread = new Thread(new AgentThreadRunnable(), "Agent daemon connecting to port "
				+ getGrinderProperties().getInt(GrinderProperties.CONSOLE_PORT, 0));
		thread.setDaemon(true);
		thread.start();
		LOGGER.info("{} is started.", thread.getName());
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	private GrinderProperties getGrinderProperties() {
		return this.properties;
	}

	class AgentThreadRunnable implements Runnable {
		public void run() {
			try {
				setAgent(new AgentImplementationEx(LOGGER, m_agentConfig)).run(getGrinderProperties());
			} catch (Exception e) {
<<<<<<< HEAD
				LOGGER.error("while running an agent thread, an error occurred", e);
=======
				LOGGER.error("While running an agent thread, an error occurred", e);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			}
			getListeners().apply(new Informer<AgentShutDownListener>() {
				public void inform(AgentShutDownListener listener) {
					listener.shutdownAgent();
				}
			});
<<<<<<< HEAD
			if (isForceToshutdown()) {
				setForceToshutdown(false);
=======
			if (isForceShutdown()) {
				setForceShutdown(false);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			}
		}
	}

	/**
	 * Interface to detect agent shutdown.
<<<<<<< HEAD
	 * 
	 * @author JunHo Yoon
	 * 
	 */
	public interface AgentShutDownListener {
		/** AgentShutdown listening method. */
=======
	 *
	 * @author JunHo Yoon
	 */
	public interface AgentShutDownListener {
		/**
		 * AgentShutdown listening method.
		 */
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		public void shutdownAgent();
	}

	public ListenerSupport<AgentShutDownListener> getListeners() {
		return this.m_listeners;
	}

	/**
	 * Reset all shutdown listener.
	 */
	public void resetListeners() {
		final ListenerSupport<AgentShutDownListener> backup = new ListenerSupport<AgentDaemon.AgentShutDownListener>();
		getListeners().apply(new Informer<AgentShutDownListener>() {
			public void inform(AgentShutDownListener listener) {
				backup.add(listener);
			}
		});

		backup.apply(new Informer<AgentShutDownListener>() {
			public void inform(AgentShutDownListener listener) {
				getListeners().remove(listener);
			}
		});
	}

	/**
	 * Add AgentShutdownListener.
<<<<<<< HEAD
	 * 
	 * @param listener
	 *            listener to detect to Agent Shutdown
=======
	 *
	 * @param listener listener to detect to Agent Shutdown
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public void addListener(AgentShutDownListener listener) {
		m_listeners.add(listener);
	}

	/**
	 * Shutdown.
	 */
	public void shutdown() {
		try {
<<<<<<< HEAD
			forceToshutdown = true;
			if (agent != null) {
				agent.shutdown();
			}
			ThreadUtil.stopQuetly(thread, "Agent Daemon is not stopped. So force to stop");
			thread = null;
		} catch (Exception e) {
			throw processException("Exception occurred while shutting down AgentDaemon", e);
		}
	}

	private boolean isForceToshutdown() {
		return forceToshutdown;
	}

	private void setForceToshutdown(boolean force) {
		this.forceToshutdown = force;
=======
			forceShutdown = true;
			if (agent != null) {
				agent.shutdown();
			}
			ThreadUtils.stopQuietly(thread, "Agent daemon is not stopped. So stop by force");
			thread = null;
		} catch (Exception e) {
			throw processException("Exception occurred while shutting down the agent daemon", e);
		}
	}

	private boolean isForceShutdown() {
		return forceShutdown;
	}

	private void setForceShutdown(boolean force) {
		this.forceShutdown = force;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

}
