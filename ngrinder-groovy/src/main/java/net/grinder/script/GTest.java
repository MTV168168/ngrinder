/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package net.grinder.script;

import java.lang.reflect.Method;

/**
 * {@link Test} extension to be distinguishable from {@link org.junit.Test} in JUnit4 tests.
<<<<<<< HEAD
 * 
=======
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * <pre>
 * request = new HTTPRequest();
 * GTest(1, &quot;Test&quot;).record(request);
 * </pre>
<<<<<<< HEAD
 * 
=======
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.2
 */
public class GTest extends Test {

<<<<<<< HEAD
	/** UID. */
=======
	/**
	 * UID.
	 */
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	private static final long serialVersionUID = 8370116882992463352L;

	/**
	 * Constructor.
<<<<<<< HEAD
	 * 
	 * @param number
	 *            the test number
	 * @param description
	 *            test description
=======
	 *
	 * @param number      the test number
	 * @param description test description
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public GTest(int number, String description) {
		super(number, description);
	}

	/**
	 * Instrument the supplied {@code target} object's method which has the given name. Subsequent
	 * calls to {@code target}'s given method will be recorded against the statistics for this
	 * {@code Test}.
<<<<<<< HEAD
	 * 
	 * @param target
	 *            Object to instrument.
	 * @param methodName
	 *            method name to instrument
	 * @throws NonInstrumentableTypeException
	 *             If {@code target} could not be instrumented.
=======
	 *
	 * @param target     Object to instrument.
	 * @param methodName method name to instrument
	 * @throws NonInstrumentableTypeException If {@code target} could not be instrumented.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @since 3.2.1
	 */
	public final void record(Object target, String methodName) throws NonInstrumentableTypeException {
		record(target, new MethodNameFilter(methodName));
	}

	/**
	 * Method name filter.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @author junoyoon
	 * @since 3.2.1
	 */
	static class MethodNameFilter implements InstrumentationFilter {
		private String methodName;

		/**
		 * Constructor.
<<<<<<< HEAD
		 * 
		 * @param methodName
		 *            method name
=======
		 *
		 * @param methodName method name
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		 */
		public MethodNameFilter(String methodName) {
			this.methodName = methodName;
		}

		@Override
		public boolean matches(Object item) {
<<<<<<< HEAD
			if (item instanceof Method) {
				return ((Method) item).getName().equals(methodName);
			}
			return false;
=======
			return item instanceof Method && ((Method) item).getName().equals(methodName);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}
}
