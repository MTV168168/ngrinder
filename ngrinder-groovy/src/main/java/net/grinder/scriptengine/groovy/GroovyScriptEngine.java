/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package net.grinder.scriptengine.groovy;

<<<<<<< HEAD
import static net.grinder.util.NoOp.noOp;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovySystem;

import java.io.IOException;

=======
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovySystem;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import net.grinder.engine.common.EngineException;
import net.grinder.engine.common.ScriptLocation;
import net.grinder.script.Grinder;
import net.grinder.script.Statistics.StatisticsForTest;
import net.grinder.scriptengine.ScriptEngineService;
import net.grinder.scriptengine.ScriptEngineService.ScriptEngine;
import net.grinder.scriptengine.ScriptExecutionException;
import net.grinder.scriptengine.exception.AbstractExceptionProcessor;
<<<<<<< HEAD
import net.grinder.scriptengine.groovy.junit.GrinderRunner;

=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.codehaus.groovy.control.CompilerConfiguration;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.model.InitializationError;

<<<<<<< HEAD
/**
 * Groovy implementation of {@link ScriptEngine}.
 * 
=======
import java.io.IOException;

import static net.grinder.util.NoOp.noOp;

/**
 * Groovy implementation of {@link ScriptEngine}.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author Ryan Gardner
 * @author JunHo Yoon (modified by)
 */
public class GroovyScriptEngine implements ScriptEngine {
	private AbstractExceptionProcessor exceptionProcessor = new GroovyExceptionProcessor();
<<<<<<< HEAD
	private Class<?> m_groovyClass;
	private GrinderContextExecutor m_grinderRunner;
	/**
	 * Construct a GroovyScriptEngine that will use the supplied ScriptLocation.
	 * 
	 * @param script
	 *            location of the .groovy script file
	 * @throws EngineException
	 *             if there is an exception loading, parsing, or constructing the test from the
	 *             file.
=======
	// For unit test, make it package protected.
	Class<?> m_groovyClass;
	private GrinderContextExecutor m_grinderRunner;

	/**
	 * Construct a GroovyScriptEngine that will use the supplied ScriptLocation.
	 *
	 * @param script location of the .groovy script file
	 * @throws EngineException if there is an exception loading, parsing, or constructing the test from the
	 *                         file.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	public GroovyScriptEngine(ScriptLocation script) throws EngineException {
		// Get groovy to compile the script and access the callable closure
		final ClassLoader parent = getClass().getClassLoader();
		CompilerConfiguration configuration = new CompilerConfiguration();
		configuration.setSourceEncoding("UTF-8");
		final GroovyClassLoader loader = new GroovyClassLoader(parent, configuration, true);
		try {
			m_groovyClass = loader.parseClass(script.getFile());
			m_grinderRunner = new GrinderContextExecutor(m_groovyClass);
			m_grinderRunner.runBeforeProcess();
			assert m_grinderRunner.testCount() > 0;
		} catch (IOException io) {
			throw new EngineException("Unable to parse groovy script at: " + script.getFile().getAbsolutePath(), io);
		} catch (InitializationError e) {
<<<<<<< HEAD
			throw new EngineException("Error while iniialize test runner", e);
		} catch (Throwable e) {
			throw new EngineException("Error while iniialize test runner", e);
=======
			throw new EngineException("Error while initialize test runner", e);
		} catch (Throwable e) {
			throw new EngineException("Error while initialize test runner", e);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ScriptEngineService.WorkerRunnable createWorkerRunnable() throws EngineException {
		try {
			return new GroovyWorkerRunnable(new GrinderContextExecutor(m_groovyClass));
		} catch (InitializationError e) {
<<<<<<< HEAD
			throw new EngineException("Exception occurred during initializing runner", e);
=======
			throw new EngineException("Exception occurred during initializing runner", exceptionProcessor.sanitize(e));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ScriptEngineService.WorkerRunnable createWorkerRunnable(Object testRunner) throws EngineException {
<<<<<<< HEAD
		return createWorkerRunnable((GrinderRunner) testRunner);
=======
		try {
			return new GroovyWorkerRunnable(new GrinderContextExecutor(m_groovyClass, testRunner));
		} catch (InitializationError e) {
			throw new EngineException("Exception occurred during initializing runner", exceptionProcessor.sanitize(e));
		}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Wrapper for groovy's testRunner closure.
	 */
	public final class GroovyWorkerRunnable implements ScriptEngineService.WorkerRunnable {
<<<<<<< HEAD
		private final GrinderContextExecutor m_groovyThreadRunner;
		private RunNotifier notifier = new RunNotifier() {
=======
		private int runCount = 0;
		private final GrinderContextExecutor m_groovyThreadRunner;
		private RunNotifier notifier = new RunNotifier() {
			@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			public void fireTestFailure(Failure failure) {
				if (exceptionProcessor.isGenericShutdown(failure.getException())) {
					if (failure.getException() instanceof RuntimeException) {
						throw (RuntimeException) failure.getException();
					} else {
						throw new RuntimeException("Wrapped", failure.getException());
					}
				}
				super.fireTestFailure(failure);
<<<<<<< HEAD
			};
=======
			}
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		};

		private GroovyWorkerRunnable(GrinderContextExecutor groovyRunner) throws EngineException {
			this.m_groovyThreadRunner = groovyRunner;
<<<<<<< HEAD
			this.m_groovyThreadRunner.runBeforeThread();
=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
			this.notifier.addListener(new RunListener() {
				@Override
				public void testFailure(Failure failure) throws Exception {
					// Skip Generic Shutdown... It's not failure.
					Throwable rootCause = exceptionProcessor.getRootCause(failure.getException());
					if (exceptionProcessor.isGenericShutdown(rootCause)) {
						return;
					}
					Grinder.grinder.getLogger().error(failure.getMessage(),
<<<<<<< HEAD
									exceptionProcessor.filterException(rootCause));
=======
							exceptionProcessor.filterException(rootCause));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					// In case of exception, set test failed.
					try {
						StatisticsForTest forLastTest = Grinder.grinder.getStatistics().getForLastTest();
						if (forLastTest != null) {
							forLastTest.setSuccess(false);
						}
					} catch (Throwable t) {
						noOp();
					}
				}
			});
<<<<<<< HEAD
=======
			this.m_groovyThreadRunner.runBeforeThread();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		}

		@Override
		public void run() throws ScriptExecutionException {
			try {
				this.m_groovyThreadRunner.run(notifier);
			} catch (RuntimeException e) {
				if (exceptionProcessor.isGenericShutdown(e)) {
					throw new GroovyScriptExecutionException("Shutdown",
<<<<<<< HEAD
									e.getMessage().equals("Wrapped") ? e.getCause() : e);
=======
							e.getMessage().equals("Wrapped") ? e.getCause() : e);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
				}
			}
		}

		@Override
		public void shutdown() throws ScriptExecutionException {
			notifier.pleaseStop();
			this.m_groovyThreadRunner.runAfterThread();
		}
	}

	/**
	 * Shut down the engine.
<<<<<<< HEAD
	 * 
	 * @throws net.grinder.engine.common.EngineException
	 *             If the engine could not be shut down.
=======
	 *
	 * @throws net.grinder.engine.common.EngineException
	 *          If the engine could not be shut down.
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 */
	@Override
	public void shutdown() throws EngineException {
		m_grinderRunner.runAfterProcess();
	}

	/**
	 * Returns a description of the script engine for the log.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return The description.
	 */
	@Override
	public String getDescription() {
		return String.format("GroovyScriptEngine running with groovy version: %s", GroovySystem.getVersion());
	}

	/**
	 * Exception thrown when an error occurs executing a GroovyScript.
	 */
	public static final class GroovyScriptExecutionException extends ScriptExecutionException {

<<<<<<< HEAD
		/** UUID. */
=======
		/**
		 * UUID.
		 */
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		private static final long serialVersionUID = -1789749790500700831L;

		/**
		 * Construct an exception with the supplied message.
<<<<<<< HEAD
		 * 
		 * @param message
		 *            the message for the exception
		 */
=======
		 *
		 * @param message the message for the exception
		 */
		@SuppressWarnings("UnusedDeclaration")
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		public GroovyScriptExecutionException(String message) {
			super(message);
		}

		/**
		 * Construct an exception with the supplied message and throwable.
<<<<<<< HEAD
		 * 
		 * @param s
		 *            the message for the exception
		 * @param t
		 *            another throwable that this exception wraps
=======
		 *
		 * @param s the message for the exception
		 * @param t another throwable that this exception wraps
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		 */
		public GroovyScriptExecutionException(String s, Throwable t) {
			super(s, t);
		}
	}
}
