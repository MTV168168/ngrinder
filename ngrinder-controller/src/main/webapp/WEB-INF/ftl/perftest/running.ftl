<<<<<<< HEAD
<div class="row">
	<div class="span5">
		<fieldSet>
			<legend>
				<@spring.message "perfTest.testRunning.summary"/>
			</legend>
		</fieldSet>
		<div class="form-horizontal form-horizontal-3" style="margin-top:10px;">
			<fieldset>
				<div class="control-group"> 
					<label class="control-label"><@spring.message "perfTest.testRunning.vusers"/></label>
					<div class="controls">
						<strong>${(test.vuserPerAgent)!}</strong>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><@spring.message "perfTest.testRunning.agents"/></label>
					<div class="controls">
						<span>${(test.agentCount)!}</span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><@spring.message "perfTest.testRunning.processes"/></label>
					<div class="controls">
						${(test.processes)!} 
						<span class="badge badge-info pull-right"><@spring.message "perfTest.testRunning.running"/> <span id="process_data"></span></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><@spring.message "perfTest.testRunning.threads"/></label>
					<div class="controls">
						${(test.threads)!} <span class="badge badge-info pull-right"><@spring.message "perfTest.testRunning.running"/> <span id="thread_data"></span></span>
					</div>
				</div>
				<hr>
				<div class="control-group">
					<label class="control-label"><@spring.message "perfTest.configuration.targetHost"/></label>
					<div class="controls">
						<#if test?exists && test.targetHosts?has_content>
							<#list test.targetHosts?split(",") as host>
								${host?trim}<br>
							</#list>
						</#if>
					</div>
				</div>
				<hr>
				<div class="control-group">
					<#if test??>
						<#if test.threshold == "D">
							<label class="control-label"> <@spring.message "perfTest.configuration.duration"/> </label>
							<div class="controls">
								<span>${(test.durationStr)!}</span>
								<code>HH:MM:SS</code>
							</div>
						<#else>
							<label class="control-label"> <@spring.message "perfTest.configuration.runCount"/> </label>
							<div class="controls">
								${(test.runCount)!}
								<span class="badge badge-success pull-right"> <span id="running_count"></span>  <@spring.message "perfTest.table.runcount"/></span>
							</div>
						</#if>
					</#if>
				</div>
				<div class="control-group">
					<label class="control-label"><@spring.message "perfTest.testRunning.targetStatus"/></label>
				</div>
				<div class="control-group">
					<div id="monitor_status" style="font-size:12px;margin-left:-20px">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><@spring.message "perfTest.testRunning.agentStatus"/></label>
				</div>
				<div class="control-group">
					<div id="agent_status" style="font-size:12px;margin-left:-20px">  
=======
<#import "../common/spring.ftl" as spring>
<#include "../common/ngrinder_macros.ftl">
<div class="row">
	<div class="span5">
		<fieldSet>
			<legend><@spring.message "perfTest.running.summaryTitle"/></legend>
		</fieldSet>
		<div class="form-horizontal form-horizontal-3" style="margin-top:10px;">
			<fieldset>
			<@control_group label_message_key="perfTest.running.totalVusers">
				<strong>${test.vuserPerAgent * test.agentCount}</strong>
				<span class="badge badge-info pull-right">
					<@spring.message "perfTest.running.running"/> <span id="running_thread"></span>
				</span>
			</@control_group>
			<@control_group label_message_key="perfTest.running.totalProcesses">
				${test.processes * test.agentCount}
				<span class="badge badge-info pull-right">
					<@spring.message "perfTest.running.running"/> <span id="running_process"></span>
				</span>
			</@control_group>
				<hr>
			<@control_group label_message_key="perfTest.config.targetHost">
				<@list list_items = test.targetHosts?split(",") ; host >
				${host?trim}<br>
				</@list>
			</@control_group>
				<hr>
				<div class="control-group">
				<#if test.threshold == "D">
					<@control_group label_message_key="perfTest.running.duration">
						<span>${test.durationStr}</span>
						<code>HH:MM:SS</code>
					  	<span class="badge badge-success pull-right">
							<span id="running_count"></span> <@spring.message "perfTest.running.runCount"/>
						</span>
					</@control_group>
				<#else>
					<@control_group label_message_key="perfTest.running.totalRunCount">
						${test.runCount * test.agentCount * test.vuserPerAgent}
						<span class="badge badge-success pull-right">
							<@spring.message "perfTest.running.runCount"/> <span id="running_count"></span>
						</span>
					</@control_group>
				</#if>
				</div>
				<div class="control-group">
					<label class="control-label"><@spring.message "perfTest.running.targetState"/></label>
				</div>
				<div class="control-group">
					<div id="monitor_state" style="font-size:12px;margin-left:-20px">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><@spring.message "perfTest.running.agentState"/></label>
				</div>
				<div class="control-group">
					<div id="agent_state" style="font-size:12px;margin-left:-20px">
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					</div>
				</div>

			</fieldset>
		</div>
	</div>
	<!-- end running content left -->
<<<<<<< HEAD
	
	<div class="span7">
		<fieldSet>
			<legend>
				<@spring.message "perfTest.testRunning.tpsStatistics"/> 
				<span class="badge badge-success" style="vertical-align:middle;">
					<@spring.message "perfTest.testRunning.runTime"/> <span id="running_time"></span>
				</span>
				<a id="stop_test_btn" class="btn btn-danger pull-right" sid="${(test.id)!}">
					<@spring.message "common.button.stop"/>
				</a>		
			</legend> 
=======

	<div class="span7">
		<fieldSet>
			<legend>
			<@spring.message "perfTest.running.tpsGraph"/>
				<span id="running_time" class="badge badge-success">&nbsp;</span>
				<a id="stop_test_btn" class="btn btn-danger pull-right">
				<@spring.message "common.button.stop"/>
				</a>
			</legend>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		</fieldSet>
		<div id="running_tps_chart" class="chart" style="width: 530px; height: 300px"></div>
		<div class="tabbable">
			<ul class="nav nav-tabs" style="" id="sample_tab">
<<<<<<< HEAD
				<li><a href="#last_sample_tab" tid="ls"><@spring.message "perfTest.testRunning.latestsample"/></a></li>
				<li><a href="#accumulated_sample_tab" tid="as"><@spring.message "perfTest.testRunning.accumulatedstatistic"/></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane" id="last_sample_tab">
=======
				<li class="active">
					<a href="#last_sample_tab"><@spring.message "perfTest.running.latestSample"/></a>
				</li>
				<li>
					<a href="#accumulated_sample_tab"><@spring.message "perfTest.running.accumulatedStatistic"/></a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="last_sample_tab">
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
					<table class="table table-striped table-bordered ellipsis" id="last_sample_table">
						<colgroup>
							<col width="30px">
							<col width="85px">
							<col width="85px">
							<col width="55px">
							<col width="60px">
							<col width="65px">
							<col width="65px">
<<<<<<< HEAD
							<col width="55px">
						</colgroup>
						<thead>
							<tr>
								<th class="no-click"><@spring.message "perfTest.testRunning.testID"/></th>
								<th class="no-click"><@spring.message "perfTest.table.testName"/></th>
								<th class="no-click"><@spring.message "perfTest.testRunning.successfulTest"/></th>
								<th class="no-click"><@spring.message "perfTest.table.errors"/></th>
								<th class="no-click" title="<@spring.message "perfTest.table.meantime"/>">MTT</th>
								<th class="no-click"><@spring.message "perfTest.table.tps"/></th>
								<th class="no-click"><@spring.message "perfTest.testRunning.responseBytePerSecond"/></th>
								<th class="no-click" title='<@spring.message "perfTest.testRunning.meanTimeToFirstByte"/>'>MTFB</th>
							</tr>
						</thead>
						<tbody>
=======
							<col width="60px">
						</colgroup>
						<thead>
						<tr>
							<th class="no-click"><@spring.message "perfTest.running.testID"/></th>
							<th class="no-click"><@spring.message "perfTest.running.testName"/></th>
							<th class="no-click"><@spring.message "perfTest.running.success"/></th>
							<th class="no-click"><@spring.message "perfTest.running.errors"/></th>
							<th class="no-click" title="<@spring.message "perfTest.running.meantime"/>">MTT</th>
							<th class="no-click"><@spring.message "perfTest.running.tps"/></th>
							<th class="no-click" title='<@spring.message "perfTest.running.meanTimeToFirstByte"/>'>
								MTFB
							</th>
							<th class="no-click" title="<@spring.message 'perfTest.running.responseBytePerSecond.full'/>">
								<@spring.message "perfTest.running.responseBytePerSecond"/>
							</th>
						</tr>
						</thead>
						<tbody id="last_sample_result">
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="accumulated_sample_tab">
<<<<<<< HEAD
					<table class="table table-striped table-bordered ellipsis" id="accumulated_sample_table"> 
=======
					<table class="table table-striped table-bordered ellipsis" id="accumulated_sample_table">
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
						<colgroup>
							<col width="30px">
							<col width="85px">
							<col width="85px">
							<col width="55px">
							<col width="60px">
							<col width="65px">
							<col width="65px">
<<<<<<< HEAD
							<col width="55px">
						</colgroup>
						<thead>
							<tr>
								<th class="no-click"><@spring.message "perfTest.testRunning.testID"/></th>
								<th class="no-click"><@spring.message "perfTest.table.testName"/></th>
								<th class="no-click"><@spring.message "perfTest.testRunning.successfulTest"/></th>
								<th class="no-click"><@spring.message "perfTest.table.errors"/></th>
								<th class="no-click" title="<@spring.message "perfTest.table.meantime"/>">MTT</th>
								<th class="no-click"><@spring.message "perfTest.table.tps"/></th>
								<th class="no-click"><@spring.message "perfTest.detail.peakTPS"/></th>
								<th class="no-click" title="<@spring.message "perfTest.testRunning.mtsd.help"/>">MTSD</th>
							</tr>
						</thead>
						<tbody>
=======
							<col width="60px">
						</colgroup>
						<thead>
						<tr>
							<th class="no-click"><@spring.message "perfTest.running.testID"/></th>
							<th class="no-click"><@spring.message "perfTest.running.testName"/></th>
							<th class="no-click"><@spring.message "perfTest.running.success"/></th>
							<th class="no-click"><@spring.message "perfTest.running.errors"/></th>
							<th class="no-click" title="<@spring.message "perfTest.running.meantime"/>">MTT</th>
							<th class="no-click"><@spring.message "perfTest.running.tps"/></th>
							<th class="no-click"
								title="<@spring.message 'perfTest.running.peakTPS.full'/>">
									<@spring.message "perfTest.running.peakTPS"/>
							</th>
							<th class="no-click"
								title="<@spring.message 'perfTest.running.responseBytePerSecond.full'/>">
									<@spring.message "perfTest.running.responseBytePerSecond"/>
							</th>
						</tr>
						</thead>
						<tbody id="accumulated_sample_result">
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- end running content right -->
</div>
<<<<<<< HEAD
<script>
	var curPeakTps = 0;
	var curTps = 0;
	var curRunningTime = 0;
	var curRunningProcesses = 0;
	var curRunningThreads = 0;
	var curRunningCount = 0;
	var curStatus = false;
	var curAgentPerfStates = [];
	var agentPerfStates = [];
	function refreshData() {
		var refreshDiv = $("<div></div>");
		var peakTps = 50;
		refreshDiv.load(
			"${req.getContextPath()}/perftest/<#if test??>${(test.id)?c}<#else>0</#if>/running/sample",
			{},
			function() {
				$("#running_time").text(showRunTime(curRunningTime));
				if (curStatus == true) {
					$("#last_sample_table tbody").html(refreshDiv.find("#last_sample_table_item"));
					$("#accumulated_sample_table tbody").html(refreshDiv.find("#accumulated_sample_table_item"));
		
					$("#process_data").text(curRunningProcesses);
					$("#thread_data").text(curRunningThreads);
					$("#running_count").text(curRunningCount);
					$("#agent_status").html(createMonitoringStatusString(curAgentStat));
					$("#monitor_status").html(createMonitoringStatusString(curMonitorStat));
					peakTps = curPeakTps;
					if (curPeakTps < 10) {
						preakTps = 10;
					}
					testTpsData.enQueue(curTps);
				} else { 
					if ($('#running_section_tab:hidden')[0]) {
						window.clearInterval(objTimer);
						return;
					} else {
						testTpsData.enQueue(0);
					}
				}
		
				if (testTpsData.getSize() > (60 / samplingInterval)) {
					testTpsData.deQueue();
				}
		
				showChart('running_tps_chart', testTpsData.aElement, peakTps, samplingInterval);
			}
		);
	}
	
	function createMonitoringStatusString(status) {
		var monitorStatusString = "<ul>";
		for ( var i = 0; i < status.length; i++) {
			var each = status[i];
			monitorStatusString = monitorStatusString + "<li class='monitor_status'><div style='wdith:100%;' class='ellipsis'><span title='" + each.agentFull + "'><b>" + each.agent + "</b></span> CPU-"
				+ each.cpu + "% MEM-" + each.mem + "% ";
			if (each.recievedPerSec != "0B" || each.sentPerSec != "0B") { 
				monitorStatusString = monitorStatusString + "/ RX-"+ each.recievedPerSec + " TX-" + each.sentPerSec + "</dv></li>";
			};
		}
		monitorStatusString += "</ul>"; 
		return monitorStatusString;
	}
	
=======

<script src="${req.getContextPath()}/js/queue.js?${nGrinderVersion}"></script>
<script>

	//@ sourceURL=/perftest/running
	var curPerf;
	var curAgentStat;
	var curMonitorStat;
	var tpsQueue = new Queue(60 / ${test.samplingInterval?c});
	var tpsChart = new Chart('running_tps_chart', [tpsQueue.getArray()], ${test.samplingInterval?c});

	var samplingAjax = new AjaxObj("/perftest/{testId}/api/sample");
	samplingAjax.params = { testId: ${(test.id!0)?c} };

	function showLastPerTestResult(container, statistics) {
		var existing = container.find("tr");
		for (var i = 0; i < statistics.length; i++) {
			var record = existing.get(i);
			if (record == undefined) {
				container.append("<tr></tr>");
				record = container.find("tr").get(i);
			}
			var output = "<td>" + toNum(statistics[i].testNumber) + "</td>";
			var testDescription = statistics[i].testDescription;
			output = output + "<td class='ellipsis' title='" + testDescription + "'>" + testDescription + "</td>";
			output = output + "<td>" + toNum(statistics[i].Tests) + "</td>";
			output = output + "<td>" + toNum(statistics[i].Errors) + "</td>";
			output = output + "<td>" + toNum(statistics[i]["Mean_Test_Time_(ms)"]) + "</td>";
			output = output + "<td>" + toNum(statistics[i].TPS) + "</td>";
			output = output + "<td>" + toNum(statistics[i].Mean_time_to_first_byte, 0) + "</td>";
			output = output + "<td>" + formatNetwork(null, statistics[i].Response_bytes_per_second) + "</td>";
			$(record).html(output);
		}
	}


	function showAccumulatedPerTestResult(container, statistics) {
		var existing = container.find("tr");
		for (var i = 0; i < statistics.length; i++) {
			var record = existing.get(i);
			if (record == undefined) {
				container.append("<tr></tr>");
				record = container.find("tr").get(i);
			}
			var output = "<td>" + toNum(statistics[i].testNumber) + "</td>";
			var testDescription = statistics[i].testDescription;
			output = output + "<td class='ellipsis' title='" + testDescription + "'>" + testDescription + "</td>";
			output = output + "<td>" + toNum(statistics[i].Tests) + "</td>";
			output = output + "<td>" + toNum(statistics[i].Errors) + "</td>";
			output = output + "<td>" + toNum(statistics[i]["Mean_Test_Time_(ms)"]) + "</td>";
			output = output + "<td>" + toNum(statistics[i].TPS) + "</td>";
			output = output + "<td>" + toNum(statistics[i].Peak_TPS) + "</td>";
			output = output + "<td>" + formatNetwork(null, statistics[i].Response_bytes_per_second) + "</td>";
			$(record).html(output);
		}
	}

	samplingAjax.success = function (res) {
		if (res.status == "TESTING") {
			/** @namespace res.perf */
			curPerf = res.perf;
			curAgentStat = res.agent;
			curMonitorStat = res.monitor;
			if (curAgentStat !== undefined) {
				$agentState.html(createMonitoringStatusString(curAgentStat));
			}
			if (curMonitorStat !== undefined) {
				$monitorState.html(createMonitoringStatusString(curMonitorStat));
			}
			if (curPerf !== undefined) {
				$runningTime.text(showRunTime(curPerf.testTime));
				$runningProcess.text($.number(curPerf.process));
				$runningThread.text($.number(curPerf.thread));
				$runningCount.text($.number(curPerf.totalStatistics.Tests + curPerf.totalStatistics.Errors));
				showLastPerTestResult($lastSampleResult, curPerf.lastSampleStatistics);
				showAccumulatedPerTestResult($accumulatedSampleResult, curPerf.cumulativeStatistics);
				tpsQueue.enQueue(curPerf.tpsChartData);
				tpsChart.plot();
			}
		} else {
			if ($('#running_section_tab:hidden')[0]) {
				window.clearInterval(objTimer);
			}
		}
	};

	samplingAjax.error = function () {
		if ($('#running_section_tab:hidden')[0]) {
			window.clearInterval(objTimer);
		}
	};

	var $runningTime = $("#running_time");
	var $runningProcess = $("#running_process");
	var $runningThread = $("#running_thread");
	var $runningCount = $("#running_count");
	var $agentState = $("#agent_state");
	var $monitorState = $("#monitor_state");
	var $accumulatedSampleResult = $("#accumulated_sample_result");
	var $lastSampleResult = $("#last_sample_result");

	function toNum(num, precision) {
		if (num == undefined) {
			return "-";
		}
		precision = precision || 0;
		return $.number(num, precision);
	}

	function createMonitoringStatusString(status) {
		var monitorStatusString = "<ul>";
		$.each(status, function (name, value) {
			monitorStatusString = monitorStatusString +
					"<li class='monitor-state' style='height:20px'><div style='width:100%;' class='ellipsis'>";
			monitorStatusString = monitorStatusString +
					"<span title='" + name + "'><b>" + getShortenString(name) + "</b></span>" +
					" CPU-" + formatPercentage(null, value.cpuUsedPercentage) +
					" MEM-" + formatPercentage(null, ((value.totalMemory - value.freeMemory) / value.totalMemory) * 100);
			if (value.receivedPerSec != 0 || value.sentPerSec != 0) {
				monitorStatusString = monitorStatusString + "/" +
						" RX-" + formatNetwork(null, value.receivedPerSec) +
						" TX-" + formatNetwork(null, value.sentPerSec) +
						"</dv></li>";
			}
		});
		monitorStatusString += "</ul>";
		return monitorStatusString;
	}

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	function showRunTime(s) {
		if (s < 60) {
			return "" + s + "s";
		}
		if (s < 3600) {
			return "" + parseInt(s / 60) + "m " + (s % 60) + "s";
		}
		if (s < 86400) {
			return "" + parseInt(s / 3600) + "h " + parseInt(s % 3600 / 60) + "m " + (s % 3600 % 60) + "s";
		}
<<<<<<< HEAD
	
		return "" + parseInt(s / 86400) + "d " + parseInt(s % 86400 / 3600) + "h " + parseInt(s % 86400 % 3600 / 60) + "m " + (s % 86400 % 3600 % 60) + "s";
	}
	
	function showChart(containerId, data, peakTps) {
		if (jqplotObj) {
			replotChart(jqplotObj, data, peakTps, undefined, samplingInterval);
		} else { 
			jqplotObj = drawChart(containerId, data, undefined, samplingInterval);
		}
	}
	
	function stopTests(ids) {
		$.ajax({
	  		url: "${req.getContextPath()}/perftest/stop",
			type: "POST",
	  		data: {"ids":ids},
			dataType:'json',
	    	success: function(res) {
	    		if (res.success) {
		    		showSuccessMsg("<@spring.message "perfTest.table.message.success.stop"/>");
	    		} else {
		    		showErrorMsg("<@spring.message "perfTest.table.message.error.stop"/>:" + res.message);
	    		}
	    	},
	    	error: function() {
	    		showErrorMsg("<@spring.message "perfTest.table.message.error.stop"/>!");
	    	}
	  	});
	}
	
	$(document).ready(function() {
		$("#stop_test_btn").click(function() {
			var id = $(this).attr("sid");
			bootbox.confirm("<@spring.message "perfTest.table.message.confirm.stop"/>", "<@spring.message "common.button.cancel"/>", "<@spring.message "common.button.ok"/>", function(result) {
				if (result) {
					stopTests(id);
				}
			});
		});
	});
</script>
=======
		return "" + parseInt(s / 86400) + "d " + parseInt(s % 86400 / 3600) + "h " + parseInt(s % 86400 % 3600 / 60) + "m " + (s % 86400 % 3600 % 60) + "s";
	}

	function stopTests(ids) {
		var ajaxObj = new AjaxPutObj("/perftest/api?action=stop",
				{ "ids" : ids },
				"<@spring.message "perfTest.message.stop.success"/>",
				"<@spring.message "perfTest.message.stop.error"/>");
		ajaxObj.call();

	}

	$("#stop_test_btn").click(function () {
		bootbox.confirm("<@spring.message "perfTest.message.stop.confirm"/>", "<@spring.message "common.button.cancel"/>", "<@spring.message "common.button.ok"/>", function (result) {
			if (result) {
				stopTests("${(test.id!0)?c}");
			}
		});
	});

	var $samplingTab = $('#sample_tab');
	$samplingTab.find('a').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	});
	$samplingTab.find('a:first').tab('show');
	samplingAjax.call();
	objTimer = window.setInterval("samplingAjax.call()", 1000 * ${test.samplingInterval?c});
</script>
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
