/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.agent.controller;

<<<<<<< HEAD
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.controller.NGrinderBaseController;
=======
import org.apache.commons.lang.StringUtils;
import org.ngrinder.common.controller.BaseController;
import org.ngrinder.common.controller.RestAPI;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.monitor.controller.model.SystemDataModel;
import org.ngrinder.monitor.share.domain.SystemInfo;
import org.ngrinder.perftest.service.monitor.MonitorInfoStore;
import org.springframework.beans.factory.annotation.Autowired;
<<<<<<< HEAD
=======
import org.springframework.http.HttpEntity;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
<<<<<<< HEAD
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.common.collect.Maps;

/**
 * Controller which gets the target host system information.
 * 
=======

import java.util.concurrent.*;

import static org.ngrinder.common.util.Preconditions.checkNotNull;

/**
 * Controller which gets the target host system information.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @since 3.2
 */
@Controller
@RequestMapping("/monitor")
<<<<<<< HEAD
public class MonitorManagerController extends NGrinderBaseController {

=======
public class MonitorManagerController extends BaseController {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Autowired
	private MonitorInfoStore monitorInfoStore;

	/**
	 * Get the target's monitor info page for the given IP.
<<<<<<< HEAD
	 * 
	 * @param model
	 *            model
	 * @param ip
	 *            target host IP
	 * @return agent/system_info
	 */
	@RequestMapping("/info")
=======
	 *
	 * @param model model
	 * @param ip    target host IP
	 * @return monitor/info
	 */
	@RequestMapping("/info")

>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	public String getMonitor(ModelMap model, @RequestParam String ip) {
		String[] addresses = StringUtils.split(ip, ":");
		if (addresses.length > 0) {
			ip = addresses[addresses.length - 1];
		}
<<<<<<< HEAD
		model.put("monitorIp", ip);
=======
		model.put("targetIP", ip);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return "monitor/info";
	}

	/**
	 * Get the target's monitored data by the given IP.
<<<<<<< HEAD
	 * 
	 * @param model
	 *            model
	 * @param ip
	 *            target host IP
	 * @return json message containing the target's monitoring data.
	 */
	@RequestMapping("/status")
	@ResponseBody
	public String getRealTimeMonitorData(ModelMap model, @RequestParam final String ip) {
		final Map<String, Object> systemInfoMap = Maps.newHashMap();
		systemInfoMap.put(JSON_SUCCESS, true);
		try {
			Future<SystemInfo> submit = Executors.newCachedThreadPool().submit(new Callable<SystemInfo>() {
				@Override
				public SystemInfo call() {
					return monitorInfoStore.getSystemInfo(ip, getConfig().getMonitorPort());
				}
			});
			SystemInfo systemInfo = submit.get(2, TimeUnit.SECONDS);
			if (systemInfo == null) {
				systemInfoMap.put(JSON_SUCCESS, false);
			} else {
				systemInfoMap.put("systemData", new SystemDataModel(systemInfo, "UNKNOWN"));
			}
		} catch (Exception e) {
			systemInfoMap.put(JSON_SUCCESS, false);
		}
		return toJson(systemInfoMap);
=======
	 *
	 * @param ip target host IP
	 * @return json message containing the target's monitoring data.
	 */
	@RequestMapping("/state")
	@RestAPI
	public HttpEntity<String> getRealTimeMonitorData(@RequestParam final String ip) throws InterruptedException, ExecutionException, TimeoutException {
		Future<SystemInfo> submit = Executors.newCachedThreadPool().submit(new Callable<SystemInfo>() {
			@Override
			public SystemInfo call() {
				return monitorInfoStore.getSystemInfo(ip, getConfig().getMonitorPort());
			}
		});
		SystemInfo systemInfo = checkNotNull(submit.get(2, TimeUnit.SECONDS), "Monitoring data is not available.");
		return toJsonHttpEntity(new SystemDataModel(systemInfo, "UNKNOWN"));
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

	/**
	 * Close the monitor JXM connection to the given target.
<<<<<<< HEAD
	 * 
	 * @param model
	 *            model
	 * @param ip
	 *            target host IP
	 * @return success if succeeded.
	 */
	@RequestMapping("/close")
	@ResponseBody
	public String closeMonitorConnection(ModelMap model, @RequestParam String ip) {
		monitorInfoStore.remove(ip);
		return returnSuccess();
=======
	 *
	 * @param ip target host IP
	 * @return success if succeeded.
	 */
	@RequestMapping("/close")
	public HttpEntity<String> closeMonitorConnection(@RequestParam String ip) {
		monitorInfoStore.close(ip);
		return successJsonHttpEntity();
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}

}
