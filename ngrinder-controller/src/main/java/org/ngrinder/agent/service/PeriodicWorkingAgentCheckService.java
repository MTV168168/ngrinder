/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.agent.service;

import java.util.Set;

import net.grinder.console.communication.AgentProcessControlImplementation;
import net.grinder.console.communication.AgentProcessControlImplementation.AgentStatus;

import org.ngrinder.extension.OnPeriodicWorkingAgentCheckRunnable;
import org.ngrinder.infra.plugin.PluginManager;
<<<<<<< HEAD
=======
import org.ngrinder.infra.schedule.ScheduledTaskService;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.ngrinder.perftest.service.AgentManager;
import org.python.google.common.base.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

<<<<<<< HEAD
/**
 * Agent periodic check service.
 * 
 * This class runs the plugins implementing
 * {@link OnPeriodicWorkingAgentCheckRunnable}.
 * 
 * It's separated from {@link AgentManagerService} to get rid of cyclic
 * injection.
 * 
=======
import javax.annotation.PostConstruct;

/**
 * Agent periodic check service.
 * <p/>
 * This class runs the plugins implementing
 * {@link OnPeriodicWorkingAgentCheckRunnable}.
 * <p/>
 * It's separated from {@link AgentManagerService} to get rid of cyclic
 * injection.
 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 * @author JunHo Yoon
 * @since 3.1.2
 */
@Service
<<<<<<< HEAD
public class PeriodicWorkingAgentCheckService {
=======
public class PeriodicWorkingAgentCheckService implements Runnable {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	@Autowired
	private PluginManager pluginManager;

	@Autowired
	private AgentManager agentManager;

<<<<<<< HEAD
	/**
	 * Run scheduled tasks checking the agent status on the currently working
	 * agents.
	 * 
	 * @since 3.1.2
	 */
	@Scheduled(fixedDelay = 2000)
	@Transactional
	public void checkWorkingAgents() {
=======
	@Autowired
	private ScheduledTaskService scheduledTaskService;

	@PostConstruct
	public void init() {
		scheduledTaskService.addFixedDelayedScheduledTaskInTransactionContext(this, 2000);
	}

	@Override
	public void run() {
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		Set<AgentStatus> workingAgents = agentManager
				.getAgentStatusSet(new Predicate<AgentProcessControlImplementation.AgentStatus>() {
					@Override
					public boolean apply(AgentStatus agentStatus) {
						return agentStatus.getConnectingPort() != 0;
					}
				});
		for (OnPeriodicWorkingAgentCheckRunnable runnable : pluginManager
				.getEnabledModulesByClass(OnPeriodicWorkingAgentCheckRunnable.class)) {
			runnable.checkWorkingAgent(workingAgents);
		}
<<<<<<< HEAD

=======
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	}
}
