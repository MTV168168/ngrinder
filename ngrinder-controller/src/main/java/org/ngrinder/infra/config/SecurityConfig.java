/* 
 * Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 */
package org.ngrinder.infra.config;

<<<<<<< HEAD
=======
import org.ngrinder.common.constant.ControllerConstants;
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/**
 * Some User want to have more secured password. Provide the enhanced pw with sha256 if a user
 * specifies ngrinder.security.sha256 in system.conf
<<<<<<< HEAD
 * 
 * @author JunHo Yoon
 * 
=======
 *
 * @author JunHo Yoon
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
 */
@Configuration
public class SecurityConfig {

	@Autowired
	private Config config;

	/**
	 * Provide the appropriate shaPasswordEncoder depending on the ngrinder.security.sha256 config.
<<<<<<< HEAD
	 * 
=======
	 *
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
	 * @return {@link ShaPasswordEncoder} with 256 if ngrinder.security.sha256=true. Otherwise
	 *         returns default {@link ShaPasswordEncoder}
	 */
	@Bean(name = "shaPasswordEncoder")
	public ShaPasswordEncoder sharPasswordEncoder() {
<<<<<<< HEAD
		boolean useEnhancedEncoding = config.getSystemProperties()
						.getPropertyBoolean("ngrinder.security.sha256", false);
=======
		boolean useEnhancedEncoding = config.getControllerProperties().getPropertyBoolean(ControllerConstants.PROP_CONTROLLER_USER_PASSWORD_SHA256);
>>>>>>> 1b78002dcaa5e3c02ccbc6da93c1b0593631c3ad
		return useEnhancedEncoding ? new ShaPasswordEncoder(256) : new ShaPasswordEncoder();
	}
}
